import curses

# Main
def main(win):
	# Store opening characters
	openChars = []
	# Store closing characters
	closeChars = {
		"(": ")",
		"{": "}",
		"[": "]",
		"<": ">" 
	}
	win.nodelay(True)
	keyPressed = None
	win.clear()
	win.addstr("Autoclose test")
	while True:
		try:
			keyPressed = win.getkey()
			# Would be neater in a switch statement
			
			# Close on page up if there is a char to close
			if keyPressed == "KEY_PPAGE" and len(openChars) > 0:
				keyPressed = closeChars[openChars.pop()]

			# Store opening characters on press
			if keyPressed in "({[<":
				openChars.append(str(keyPressed))

			# Clear saved chars on ctrl+a
			if keyPressed == "^A":
				openChars = []

			# Backspace
			if keyPressed == "KEY_BACKSPACE":
				# Store empty string
				keyPressed = ""
				# Get position of cursor
				cursorPos = win.getyx()
				# Delete char at cursorYpos, cursorXpos - 1
				win.delch(cursorPos[0], cursorPos[1] - 1)	
			
			# Print key
			win.addstr(str(keyPressed))

		except Exception as e:
			# No input
			pass

if __name__ == "__main__":
	curses.wrapper(main)