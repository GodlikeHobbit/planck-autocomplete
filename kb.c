#include <stdlib.h>
#include <ctype.h>
#include <curses.h>
#include <string.h>

#ifndef CTRL
#define	CTRL(c)	(c & 037)
#endif

#define ARR_SIZE 1024

char * intprtkey(int ch);


int main(void) {

	WINDOW * mainwin;
	int ch;
	/*  Initialize ncurses  */
	if ( (mainwin = initscr()) == NULL ) {
		fprintf(stderr, "Error initializing ncurses.\n");
		exit(EXIT_FAILURE);
	}
	noecho();
	keypad(mainwin, TRUE);
	/*  Loop until user presses 'q'  */
	while (true) {
		ch = getch(); 
		addstr(intprtkey(ch));
	}
	/*  Clean up after ourselves  */
	delwin(mainwin);
	endwin();
	refresh();
	return EXIT_SUCCESS;
}

char elements[ARR_SIZE];
int top = -1;

void push( char ch)
{
	elements[ ++top] = ch;
}

char pop()
{
	return elements[ top--];
}

int isEmpty()
{
	return top == -1;
}

int isFull()
{
	return top == ARR_SIZE - 1;
}


/*  Returns a string describing a character passed to it  */
char * intprtkey(int ch) {
	static char keych[1] = {0};
	// If a key that was not a letter was pressed
	if (ch & KEY_CODE_YES) {
		if (ch == KEY_PPAGE) {
			// Page up
			if ( !isEmpty() ) {
				keych[0] = pop();
			} else {
				keych[0] = *"";
			}
		} else if (ch == KEY_BACKSPACE) {
			// Backspace
			int y = 0, x = 0;
			// Get position of cursor
			getyx(curscr, y, x);
			// Delete char at cursorYpos, cursorXpos - 1
			mvdelch(y, x - 1);
			keych[0] = *"";
		} else {
			// Other
			keych[0] = *"";
		}
	} else {
		// A letter key was pressed
		switch (ch) {
			// If ctrl+a
			case CTRL('a'):
				memset(elements, 0, sizeof elements);
				break;
			// (
			case 40:
				push(*")");
				break;
			// {
			case 123:
				push(*"}");
				break;
			// [
			case 91:
				push(*"]");
				break;
			// <
			case 60:
				push(*">");
				break;
		}
		keych[0] = ch;
	}
	return keych;
}